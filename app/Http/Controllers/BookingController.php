<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class BookingController extends Controller
{

    /**
     * Show the job application.
     *
     * @return \Illuminate\Http\Response
     */
    public function booking()
    {
        return view('pages.booking');
    }

}
