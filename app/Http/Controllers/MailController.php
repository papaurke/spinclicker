<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class MailController extends Controller
{

    /*
    |--------------------------------------------------------------------------
    | Send Email
    |--------------------------------------------------------------------------
    |
    | Sending email from home page, contact section
    |
    */
    public function sendEmail(Request $request)
    {
        Mail::send('emails.contact', ['title' => 'Spinclicker - Message from ' . $request->name, 'content' => $request->message], function ($message) use ($request) {
            $message->from($request->email);
            $message->subject('Spinclicker - Message from ' . $request->name . ' email: ' . $request->email);
            $message->to('spinclicker@gmail.com');
        });

        return 'Success';

    }

}
