@extends('index')

@section('content')
    <!-- Navigation -->
    <nav class="navbar navbar-expand-lg navbar-dark fixed-top" id="mainNav">
        <div class="container">
            <a class="navbar-brand js-scroll-trigger" href="#page-top">Spinclicker</a>
            <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                Menu
                <i class="fa fa-bars"></i>
            </button>
            <div class="collapse navbar-collapse" id="navbarResponsive">
                <ul class="navbar-nav text-uppercase ml-auto">
                    <li class="nav-item">
                        <a class="nav-link js-scroll-trigger" href="#description">Description</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link js-scroll-trigger" href="#portfolio">Gallery</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link js-scroll-trigger" href="#contact">Contact</a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>

    <!-- Header -->
    <header class="masthead">
        <div class="container">
            <div class="intro-text">
                <div class="intro-lead-in">Welcome To Our Website!</div>
                <div class="intro-heading text-uppercase">Spinclicker</div>
                <a class="btn btn-primary btn-xl text-uppercase js-scroll-trigger header-button" href="#description">Tell Me More</a>
            </div>
        </div>
    </header>

    <!-- Services -->
    <section id="description">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h2 class="section-heading text-uppercase">What is Spinclicker?</h2>
                    <h3 class="section-subheading text-muted">Spinclicker is a string of beads manipulated
                        with one or two hands and is used for relaxation, stopping anxiety and limiting smoking.
                        Our worry beads originate from komboloi.
                        There are many different ways to use worrying beads and everyone finds
                        a unique routine that works just for them. Some people spin the beads in their hands.
                        Others choose to count the beads over and over. There is no right or wrong way.
                        The goal is simply to use the beads to defuse stress and calm the nerves.
                    </h3>
                </div>
            </div>
            <div class="row text-center">
                <div class="col-md-4">
            <span class="fa-stack fa-4x">
              <i class="fa fa-circle fa-stack-2x text-primary"></i>
              <i class="fa fa-child fa-stack-1x fa-inverse"></i>
            </span>
                    <h4 class="service-heading">Stress Relief</h4>
                    <p class="text-muted">Relaxation, enjoyment, and generally passing the time</p>
                </div>
                <div class="col-md-4">
            <span class="fa-stack fa-4x">
              <i class="fa fa-circle fa-stack-2x text-primary"></i>
              <i class="fa fa-smoking-ban fa-stack-1x fa-inverse"></i>
            </span>
                    <h4 class="service-heading">Anti Smoking</h4>
                    <p class="text-muted">Used by people who wish to limit smoking</p>
                </div>
                <div class="col-md-4">
            <span class="fa-stack fa-4x">
              <i class="fa fa-circle fa-stack-2x text-primary"></i>
              <i class="fa fa-sign-language fa-stack-1x fa-inverse"></i>
            </span>
                    <h4 class="service-heading">Usage</h4>
                    <p class="text-muted">Spinclicker can be used as an amulet, to guard against bad luck, and as a mark of power and social prestige.</p>
                </div>
            </div>
        </div>
    </section>

    <!-- Gallery Grid -->
    <section class="bg-light" id="portfolio">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h2 class="section-heading text-uppercase">Gallery Coming Soon</h2>
                    <h3 class="section-subheading text-muted">All of our products are made of natural materials, take a look in our gallery.</h3>
                </div>
            </div>
            {{--<div class="row">--}}
                {{--<div class="col-md-4 col-sm-6 portfolio-item">--}}
                    {{--<a class="portfolio-link" data-toggle="modal" href="#portfolioModal1">--}}
                        {{--<div class="portfolio-hover">--}}
                            {{--<div class="portfolio-hover-content">--}}
                                {{--<i class="fa fa-plus fa-3x"></i>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                        {{--<img class="img-fluid" src="images/portfolio/01-thumbnail.jpg" alt="">--}}
                    {{--</a>--}}
                    {{--<div class="portfolio-caption">--}}
                        {{--<h4>Threads</h4>--}}
                        {{--<p class="text-muted">Illustration</p>--}}
                    {{--</div>--}}
                {{--</div>--}}
                {{--<div class="col-md-4 col-sm-6 portfolio-item">--}}
                    {{--<a class="portfolio-link" data-toggle="modal" href="#portfolioModal2">--}}
                        {{--<div class="portfolio-hover">--}}
                            {{--<div class="portfolio-hover-content">--}}
                                {{--<i class="fa fa-plus fa-3x"></i>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                        {{--<img class="img-fluid" src="images/portfolio/02-thumbnail.jpg" alt="">--}}
                    {{--</a>--}}
                    {{--<div class="portfolio-caption">--}}
                        {{--<h4>Explore</h4>--}}
                        {{--<p class="text-muted">Graphic Design</p>--}}
                    {{--</div>--}}
                {{--</div>--}}
                {{--<div class="col-md-4 col-sm-6 portfolio-item">--}}
                    {{--<a class="portfolio-link" data-toggle="modal" href="#portfolioModal3">--}}
                        {{--<div class="portfolio-hover">--}}
                            {{--<div class="portfolio-hover-content">--}}
                                {{--<i class="fa fa-plus fa-3x"></i>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                        {{--<img class="img-fluid" src="images/portfolio/03-thumbnail.jpg" alt="">--}}
                    {{--</a>--}}
                    {{--<div class="portfolio-caption">--}}
                        {{--<h4>Finish</h4>--}}
                        {{--<p class="text-muted">Identity</p>--}}
                    {{--</div>--}}
                {{--</div>--}}
                {{--<div class="col-md-4 col-sm-6 portfolio-item">--}}
                    {{--<a class="portfolio-link" data-toggle="modal" href="#portfolioModal4">--}}
                        {{--<div class="portfolio-hover">--}}
                            {{--<div class="portfolio-hover-content">--}}
                                {{--<i class="fa fa-plus fa-3x"></i>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                        {{--<img class="img-fluid" src="images/portfolio/04-thumbnail.jpg" alt="">--}}
                    {{--</a>--}}
                    {{--<div class="portfolio-caption">--}}
                        {{--<h4>Lines</h4>--}}
                        {{--<p class="text-muted">Branding</p>--}}
                    {{--</div>--}}
                {{--</div>--}}
                {{--<div class="col-md-4 col-sm-6 portfolio-item">--}}
                    {{--<a class="portfolio-link" data-toggle="modal" href="#portfolioModal5">--}}
                        {{--<div class="portfolio-hover">--}}
                            {{--<div class="portfolio-hover-content">--}}
                                {{--<i class="fa fa-plus fa-3x"></i>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                        {{--<img class="img-fluid" src="images/portfolio/05-thumbnail.jpg" alt="">--}}
                    {{--</a>--}}
                    {{--<div class="portfolio-caption">--}}
                        {{--<h4>Southwest</h4>--}}
                        {{--<p class="text-muted">Website Design</p>--}}
                    {{--</div>--}}
                {{--</div>--}}
                {{--<div class="col-md-4 col-sm-6 portfolio-item">--}}
                    {{--<a class="portfolio-link" data-toggle="modal" href="#portfolioModal6">--}}
                        {{--<div class="portfolio-hover">--}}
                            {{--<div class="portfolio-hover-content">--}}
                                {{--<i class="fa fa-plus fa-3x"></i>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                        {{--<img class="img-fluid" src="images/portfolio/06-thumbnail.jpg" alt="">--}}
                    {{--</a>--}}
                    {{--<div class="portfolio-caption">--}}
                        {{--<h4>Window</h4>--}}
                        {{--<p class="text-muted">Photography</p>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}
        </div>
    </section>

    {{--<!-- Clients -->--}}
    {{--<section class="py-5">--}}
        {{--<div class="container">--}}
            {{--<div class="row">--}}
                {{--<div class="col-md-3 col-sm-6">--}}
                    {{--<a href="#">--}}
                        {{--<img class="img-fluid d-block mx-auto" src="images/logos/envato.jpg" alt="">--}}
                    {{--</a>--}}
                {{--</div>--}}
                {{--<div class="col-md-3 col-sm-6">--}}
                    {{--<a href="#">--}}
                        {{--<img class="img-fluid d-block mx-auto" src="images/logos/designmodo.jpg" alt="">--}}
                    {{--</a>--}}
                {{--</div>--}}
                {{--<div class="col-md-3 col-sm-6">--}}
                    {{--<a href="#">--}}
                        {{--<img class="img-fluid d-block mx-auto" src="images/logos/themeforest.jpg" alt="">--}}
                    {{--</a>--}}
                {{--</div>--}}
                {{--<div class="col-md-3 col-sm-6">--}}
                    {{--<a href="#">--}}
                        {{--<img class="img-fluid d-block mx-auto" src="images/logos/creative-market.jpg" alt="">--}}
                    {{--</a>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
    {{--</section>--}}

    <!-- Contact -->
    <section id="contact">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h2 class="section-heading text-uppercase">Contact Us</h2>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <form id="contactForm" name="sentMessage" novalidate="novalidate">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <input class="form-control" id="name" type="text" placeholder="Your Name *" required="required" data-validation-required-message="Please enter your name.">
                                    <p class="help-block text-danger"></p>
                                </div>
                                <div class="form-group">
                                    <input class="form-control" id="email" type="email" placeholder="Your Email *" required="required" data-validation-required-message="Please enter your email address.">
                                    <p class="help-block text-danger"></p>
                                </div>
                                <div class="form-group">
                                    <input class="form-control" id="phone" type="tel" placeholder="Your Phone *" required="required" data-validation-required-message="Please enter your phone number.">
                                    <p class="help-block text-danger"></p>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <textarea class="form-control" id="message" placeholder="Your Message *" required="required" data-validation-required-message="Please enter a message."></textarea>
                                    <p class="help-block text-danger"></p>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="col-lg-12 text-center">
                                <div id="success"></div>
                                <button id="sendMessageButton" class="btn btn-primary btn-xl text-uppercase" type="submit">Send Message</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>

    <!-- Footer -->
    <footer>
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <span class="copyright">Copyright &copy; Spinclicker 2018</span>
                </div>
                <div class="col-md-4">
                    <ul class="list-inline social-buttons">
                        <li class="list-inline-item">
                            <a href="https://www.instagram.com/spinclicker/" target="_blank">
                                <i class="fab fa-instagram"></i>
                            </a>
                        </li>
                        <li class="list-inline-item">
                            <a href="#" target="_blank">
                                <i class="fab fa-facebook"></i>
                            </a>
                        </li>
                        <li class="list-inline-item">
                            <a href="" target="_blank">
                                <i class="fab fa-twitter"></i>
                            </a>
                        </li>
                        <li class="list-inline-item">
                            <a href="#" target="_blank">
                                <i class="fab fa-linkedin"></i>
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="col-md-4">
                    <span class="copyright">Site Developed By &copy; <a target="_blank" href="http://www.urosmiric.com">www.urosmiric.com</a></span>
                </div>
                {{--<div class="col-md-4">--}}
                    {{--<ul class="list-inline quicklinks">--}}
                        {{--<li class="list-inline-item">--}}
                            {{--<a href="#">Privacy Policy</a>--}}
                        {{--</li>--}}
                        {{--<li class="list-inline-item">--}}
                            {{--<a href="#">Terms of Use</a>--}}
                        {{--</li>--}}
                    {{--</ul>--}}
                {{--</div>--}}
            </div>
        </div>
    </footer>

    <!-- Portfolio Modals -->

    <!-- Modal 1 -->
    <div class="portfolio-modal modal fade" id="portfolioModal1" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="close-modal" data-dismiss="modal">
                    <div class="lr">
                        <div class="rl"></div>
                    </div>
                </div>
                <div class="container">
                    <div class="row">
                        <div class="col-lg-8 mx-auto">
                            <div class="modal-body">
                                <!-- Project Details Go Here -->
                                <h2 class="text-uppercase">Project Name</h2>
                                <p class="item-intro text-muted">Lorem ipsum dolor sit amet consectetur.</p>
                                <img class="img-fluid d-block mx-auto" src="images/portfolio/01-full.jpg" alt="">
                                <p>Use this area to describe your project. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Est blanditiis dolorem culpa incidunt minus dignissimos deserunt repellat aperiam quasi sunt officia expedita beatae cupiditate, maiores repudiandae, nostrum, reiciendis facere nemo!</p>
                                <ul class="list-inline">
                                    <li>Date: January 2017</li>
                                    <li>Client: Threads</li>
                                    <li>Category: Illustration</li>
                                </ul>
                                <button class="btn btn-primary" data-dismiss="modal" type="button">
                                    <i class="fa fa-times"></i>
                                    Close Project</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal 2 -->
    <div class="portfolio-modal modal fade" id="portfolioModal2" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="close-modal" data-dismiss="modal">
                    <div class="lr">
                        <div class="rl"></div>
                    </div>
                </div>
                <div class="container">
                    <div class="row">
                        <div class="col-lg-8 mx-auto">
                            <div class="modal-body">
                                <!-- Project Details Go Here -->
                                <h2 class="text-uppercase">Project Name</h2>
                                <p class="item-intro text-muted">Lorem ipsum dolor sit amet consectetur.</p>
                                <img class="img-fluid d-block mx-auto" src="images/portfolio/02-full.jpg" alt="">
                                <p>Use this area to describe your project. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Est blanditiis dolorem culpa incidunt minus dignissimos deserunt repellat aperiam quasi sunt officia expedita beatae cupiditate, maiores repudiandae, nostrum, reiciendis facere nemo!</p>
                                <ul class="list-inline">
                                    <li>Date: January 2017</li>
                                    <li>Client: Explore</li>
                                    <li>Category: Graphic Design</li>
                                </ul>
                                <button class="btn btn-primary" data-dismiss="modal" type="button">
                                    <i class="fa fa-times"></i>
                                    Close Project</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal 3 -->
    <div class="portfolio-modal modal fade" id="portfolioModal3" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="close-modal" data-dismiss="modal">
                    <div class="lr">
                        <div class="rl"></div>
                    </div>
                </div>
                <div class="container">
                    <div class="row">
                        <div class="col-lg-8 mx-auto">
                            <div class="modal-body">
                                <!-- Project Details Go Here -->
                                <h2 class="text-uppercase">Project Name</h2>
                                <p class="item-intro text-muted">Lorem ipsum dolor sit amet consectetur.</p>
                                <img class="img-fluid d-block mx-auto" src="images/portfolio/03-full.jpg" alt="">
                                <p>Use this area to describe your project. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Est blanditiis dolorem culpa incidunt minus dignissimos deserunt repellat aperiam quasi sunt officia expedita beatae cupiditate, maiores repudiandae, nostrum, reiciendis facere nemo!</p>
                                <ul class="list-inline">
                                    <li>Date: January 2017</li>
                                    <li>Client: Finish</li>
                                    <li>Category: Identity</li>
                                </ul>
                                <button class="btn btn-primary" data-dismiss="modal" type="button">
                                    <i class="fa fa-times"></i>
                                    Close Project</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal 4 -->
    <div class="portfolio-modal modal fade" id="portfolioModal4" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="close-modal" data-dismiss="modal">
                    <div class="lr">
                        <div class="rl"></div>
                    </div>
                </div>
                <div class="container">
                    <div class="row">
                        <div class="col-lg-8 mx-auto">
                            <div class="modal-body">
                                <!-- Project Details Go Here -->
                                <h2 class="text-uppercase">Project Name</h2>
                                <p class="item-intro text-muted">Lorem ipsum dolor sit amet consectetur.</p>
                                <img class="img-fluid d-block mx-auto" src="images/portfolio/04-full.jpg" alt="">
                                <p>Use this area to describe your project. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Est blanditiis dolorem culpa incidunt minus dignissimos deserunt repellat aperiam quasi sunt officia expedita beatae cupiditate, maiores repudiandae, nostrum, reiciendis facere nemo!</p>
                                <ul class="list-inline">
                                    <li>Date: January 2017</li>
                                    <li>Client: Lines</li>
                                    <li>Category: Branding</li>
                                </ul>
                                <button class="btn btn-primary" data-dismiss="modal" type="button">
                                    <i class="fa fa-times"></i>
                                    Close Project</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal 5 -->
    <div class="portfolio-modal modal fade" id="portfolioModal5" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="close-modal" data-dismiss="modal">
                    <div class="lr">
                        <div class="rl"></div>
                    </div>
                </div>
                <div class="container">
                    <div class="row">
                        <div class="col-lg-8 mx-auto">
                            <div class="modal-body">
                                <!-- Project Details Go Here -->
                                <h2 class="text-uppercase">Project Name</h2>
                                <p class="item-intro text-muted">Lorem ipsum dolor sit amet consectetur.</p>
                                <img class="img-fluid d-block mx-auto" src="images/portfolio/05-full.jpg" alt="">
                                <p>Use this area to describe your project. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Est blanditiis dolorem culpa incidunt minus dignissimos deserunt repellat aperiam quasi sunt officia expedita beatae cupiditate, maiores repudiandae, nostrum, reiciendis facere nemo!</p>
                                <ul class="list-inline">
                                    <li>Date: January 2017</li>
                                    <li>Client: Southwest</li>
                                    <li>Category: Website Design</li>
                                </ul>
                                <button class="btn btn-primary" data-dismiss="modal" type="button">
                                    <i class="fa fa-times"></i>
                                    Close Project</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal 6 -->
    <div class="portfolio-modal modal fade" id="portfolioModal6" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="close-modal" data-dismiss="modal">
                    <div class="lr">
                        <div class="rl"></div>
                    </div>
                </div>
                <div class="container">
                    <div class="row">
                        <div class="col-lg-8 mx-auto">
                            <div class="modal-body">
                                <!-- Project Details Go Here -->
                                <h2 class="text-uppercase">Project Name</h2>
                                <p class="item-intro text-muted">Lorem ipsum dolor sit amet consectetur.</p>
                                <img class="img-fluid d-block mx-auto" src="images/portfolio/06-full.jpg" alt="">
                                <p>Use this area to describe your project. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Est blanditiis dolorem culpa incidunt minus dignissimos deserunt repellat aperiam quasi sunt officia expedita beatae cupiditate, maiores repudiandae, nostrum, reiciendis facere nemo!</p>
                                <ul class="list-inline">
                                    <li>Date: January 2017</li>
                                    <li>Client: Window</li>
                                    <li>Category: Photography</li>
                                </ul>
                                <button class="btn btn-primary" data-dismiss="modal" type="button">
                                    <i class="fa fa-times"></i>
                                    Close Project</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection
