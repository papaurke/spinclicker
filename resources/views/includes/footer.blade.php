<!-- JS -->
<script type="text/javascript" src="{{ URL::asset('/js/jquery.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('/js/bootstrap.bundle.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('/js/jquery.easing.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('/js/jqBootstrapValidation.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('/js/agency.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('/js/contact_me.js') }}"></script>

</body>
</html>
