@extends('index')

@section('content')
    <div id="page">
        <div class="page-header">
            <h1>Booking</h1>
        </div>
        <div id="calendar"></div>
    </div>

    <div class="bubble">
        <div class="arrow"></div>
    </div>
@endsection